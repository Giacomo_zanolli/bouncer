import 'package:flutter_test/flutter_test.dart';

import 'package:bouncer/main.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  testWidgets('The app opens', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());
  });
}
