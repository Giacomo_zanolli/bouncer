import 'dart:math'; //for sin() and cos()

import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      showPerformanceOverlay: true,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SizedBox.expand(
          child: RadialMenu(),
        ),
        backgroundColor: Color(0xffffffff),
      ),
    );
  }
}

class RadialMenu extends StatefulWidget {
  @override
  _RadialMenuState createState() => _RadialMenuState();
}

class _RadialMenuState extends State<RadialMenu>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(duration: Duration(milliseconds: 500), vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return RadialAnimation(controller: animationController);
  }
}

class RadialAnimation extends StatelessWidget {
  final AnimationController controller;
  final Animation<double>
      scale; //used for scaling the central button in and out
  final Animation<double>
      translation; //used to bring the outer icons out from the center (translates radilly)
  final Animation<double>
      rotation; //used to rotate the whole frame while animating

//list of the colors for the icons of the outer layer
  final List<Color> outerIconsColors = [
    Colors.red,
    Colors.green,
    Colors.orange,
    Colors.blue,
    Colors.black,
    Colors.indigo,
    Colors.pink,
    Colors.yellow,
  ];
//list of the icons that will form the outer layer
  final List<IconData> outerIcons = [
    FontAwesomeIcons.thumbtack,
    FontAwesomeIcons.sprayCan,
    FontAwesomeIcons.fire,
    FontAwesomeIcons.kiwiBird,
    FontAwesomeIcons.cat,
    FontAwesomeIcons.paw,
    FontAwesomeIcons.bong,
    FontAwesomeIcons.bolt,
  ];

  RadialAnimation({Key key, this.controller})
      :
        //tween generates the values in between begin and end, according to the animation controller
        scale = Tween<double>(
          begin: 1.2,
          end: 0.0,
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Curves.fastOutSlowIn,
          ),
        ),
        translation = Tween<double>(begin: 0, end: 110).animate(
          CurvedAnimation(
            parent: controller,
            curve: Curves.linear,
          ),
        ),
        //controls the rotation that will be applied to the whole frame
        rotation = Tween<double>(
          begin: 360.0,
          end: 0.0,
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.0,
              0.7, //l'animazione andrà dall'inizio dell'animazione(0) fino al 70% della sua durata(0.7)
              curve: Curves.decelerate,
            ),
          ),
        ),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller,
      builder: (BuildContext context, Widget builder) {
        return Transform.rotate(
          angle: _toRadians(rotation.value),
          child: Stack(
            alignment: Alignment.center,
            children: _buildButtons(outerIcons, outerIconsColors) +
                [
                  Transform.scale(
                    scale: scale.value - 1.2, //this makes the close button shrink while the other is expanding and vice-versa
                    child: FloatingActionButton(
                      child: Icon(
                        FontAwesomeIcons.timesCircle,
                      ),
                      backgroundColor: Colors.red,
                      onPressed: _close,
                    ),
                  ),
                  Transform.scale(
                    scale: scale.value,
                    child: FloatingActionButton(
                      child: Icon(FontAwesomeIcons.solidDotCircle),
                      onPressed: _open,
                    ),
                  ),
                ],
          ),
        );
      },
    );
  }

  ///Returns the buttons starting from the lists of `icons` and `colors`
  ///the two lists **must** have the same length
  List<Widget> _buildButtons(List<IconData> icons, List<Color> colors) {
    assert(icons.length == colors.length);
    List<Widget> result = [];
    double angle = 0;
    double increment = 360 / icons.length;
    //adding one icon at the time starting from angle 0 and incrementing it based on the number of icons to space them equally
    for (int i = 0; i < icons.length; i++) {
      result.add(
        _buildButton(
          angle,
          backgroundColor: colors[i],
          icon: icons[i],
        ),
      );
      angle += increment;
    }
    return result;
  }

  ///Generates the outer button
  ///The `translation` is also applied here in order to animate the buttons
  _buildButton(double angle,
      {@required Color backgroundColor, @required IconData icon}) {
    final double rad = _toRadians(angle);
    return Transform(
      transform: Matrix4.identity()
        ..translate(
          (translation.value) * cos(rad),
          (translation.value) * sin(rad),
        ),
      child: FloatingActionButton(
        child: Icon(icon),
        backgroundColor: backgroundColor,
        onPressed: () {},
      ),
    );
  }

  ///causes the controller to play the animation
  _open() {
    controller.forward();
  }

  ///causes the controller to play the animation reversed
  _close() {
    controller.reverse();
  }

  ///Converts the angle from degrees to radiants, didn't want to make an import just for that
  _toRadians(double angle) {
    return angle / 360 * 2 * pi;
  }
}
